const mongoose = require('mongoose');
const config = require('./config');
const Artist = require("./models/Artist");
const Album = require("./models/Album");
const run = async () => {

    await mongoose.connect(config.mongo.db, config.mongo.options);
    const collections = await mongoose.connection.db.listCollections().toArray()

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [Eminem, Rihanna] = await Artist.create({
            name: 'Eminem',
            information: 'American rapper, record producer, and actor who was known as one of the most-controversial and best-selling artists of the early 21st century.',
            photo: 'eminemPhoto.jpg'
        },
        {
            name: 'Rihanna',
            information: 'Barbadian pop and rhythm-and-blues (R&B) singer who became a worldwide star in the early 21st century',
            photo: 'rihannaPhoto.jpg'
        });

    await Album.create({
            artist: Eminem,
            title: 'The Eminem Show',
            date: '2002',
            image: 'eminem2.jpg'
        },
        {
            artist: Eminem,
            title: 'The Marshall Mathers LP',
            date: '2000',
            image: 'eminem1.jpg'
        },
        {
            artist: Rihanna,
            title: 'Anti',
            date: '2016',
            image: 'rihanna1.jpg'
        }
    )


    await mongoose.connection.close();
}
run().catch(e => console.error(e))