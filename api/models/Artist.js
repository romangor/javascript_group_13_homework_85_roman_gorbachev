const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ArtistSchema = new Schema({
    name: String,
    information: String,
    photo: String,
});

const Artist = mongoose.model('Artist', ArtistSchema);

module.exports = Artist;