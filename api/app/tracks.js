const express = require('express');
const mongoose = require("mongoose");
const Track = require("../models/Track");
const router = express.Router();

router.get('/', async (req, res, next) => {
   try {
       const sort = {}
        if(req.query.params){
            sort.album = req.query.album
          const tracks = await Track.find().sort(sort);
          res.send(tracks);
        }
        const tracks = await Track.find().populate("album", "title lengthOfTrack");
        res.send(tracks);
   } catch (e) {
       next(e);
   }
});

router.post('/', async (req, res, next) => {
    try {
        const track = new Track (req.body);
        await track.save();
        res.send({message: 'Created new track ', track});
    } catch (e) {
        if (e instanceof mongoose.Error.ValidationError) {
            return res.status(400).send(e);
        }
        return next(e);
    }
});

module.exports = router;
