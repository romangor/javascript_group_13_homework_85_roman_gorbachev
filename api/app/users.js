const express = require('express');
const mongoose = require("mongoose");
const User = require("../models/User");


const router = express.Router();

router.post('/', async (req, res, next) => {
    try {
        const user = new User(req.body);
        await user.save();
        return res.send({message: 'Created user ', user});
    } catch (e) {
        if (e instanceof mongoose.Error.ValidationError) {
            return res.status(400).send(e);
        }
            return next(e);
    }
});

router.post('/sessions', async (req, res, next) => {
   try {
    const user = await User.findOne({userName: req.body.userName});

    if(!user) {
        return res.status(400).send({message: 'User is not found'})
    }
    const isMatch = await user.checkPassword(req.body.password)
    console.log(isMatch)
       if (!isMatch) {
        return res.status(400).send('Password is incorrect');
    }
    user.generateToken();
    await user.save();

    return res.send( {token: user.token});

   } catch (e) {
       next(e);
   }
});


module.exports = router;