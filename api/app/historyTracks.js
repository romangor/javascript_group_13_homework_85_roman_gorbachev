const express = require('express');
const auth = require("../middleware/auth");
const TrackHistory = require("../models/TrackHistory");
const router = express.Router();

router.post('/', auth, async (req, res, next) => {
   try {
       const dateTime = new Date().toString();
       const data = {
           userName: req.user.userName,
           track : req.body.track,
           dateTime : dateTime
       }
       const trackHistory = new TrackHistory(data);
       await trackHistory.save();
       res.send({message: 'Added an entry to the track listening history', trackHistory});
   } catch (e) {
       next(e);
   }

});

module.exports = router;